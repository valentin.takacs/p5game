class Patrat {

  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.width = 119;
    this.height = 119;
    this.roundness = 20;
    this.stare = STARE_GOL;
  }

  display() {
    if (this.stare == STARE_GOL) {
      fill('#ffff00');
      strokeWeight(3);
      rect(this.x, this.y, this.width, this.height, this.roundness);
    } else if (this.stare == STARE_X) {
      fill('#ffff00');
      rect(this.x, this.y, this.width, this.height, this.roundness);
      push();
      stroke(0);
      strokeWeight(5);
      line(this.x + this.roundness,
        this.y + this.roundness,
        this.x + this.width - this.roundness,
        this.y + this.height - this.roundness);
      line(this.width - this.roundness + this.x,
        this.y + this.roundness,
        this.x + this.roundness,
        this.y + this.height - this.roundness);
      pop();
    } else if (this.stare == STARE_O) {
      fill('#ffff00');
      rect(this.x, this.y, this.width, this.height, this.roundness);

      push();
      fill('#ffff00');
      strokeWeight(5);
      circle(this.x + this.width / 2, this.y + this.height / 2, 100);
      pop();
    } 
  }


}