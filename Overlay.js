class Overlay {

  constructor() {

    let button;
    this.p1input = createInput();
    this.p2input = createInput();

    this.buttonRematch = createButton('Rematch');
    this.buttonRematch.mousePressed(() => { //functie lambda/arrow 
      RAND=RAND_X;
      refreshGame();
      this.hideWinnerScreen();
    });
    this.buttonRematch.position(525, 480);
    this.buttonRematch.style('font-size', '30px');
    this.buttonRematch.style('background-color', 'red');
    this.buttonRematch.size(200, 100);
    this.buttonRematch.hide();

    this.buttonNewGame = createButton('New Game');
    this.buttonNewGame.mousePressed(() => {
      RAND=RAND_X;
      this.p1input.value('');
      this.p2input.value('');
      this.p1input.show();
      this.p2input.show();
      button.show();
      refreshGame();
      SHOW_OVERLAY = true;
      this.hideWinnerScreen();
    });
    this.buttonNewGame.position(525, 600);
    this.buttonNewGame.style('font-size', '30px');
    this.buttonNewGame.style('background-color', 'red');
    this.buttonNewGame.size(200, 100);
    this.buttonNewGame.hide();

    this.p1nume = "";
    this.p2nume = "";

    button = createButton('Play');
    button.style('font-size', '30px');
    button.style('background-color', 'red');
    button.position(500, 400);
    button.size(200, 100);

    button.mousePressed(() => {
      console.log(this.p1input.value());
      if (this.p1input.value() == "" || this.p1input.value() == "" 
      || this.p1input.value()==this.p2input.value()) {
        //nu a tastat ok numele jucatorilor
      } else {
        SHOW_OVERLAY = false;
        button.hide();
        this.p1input.hide();
        this.p2input.hide();
        this.buttonRematch.hide();

        this.p1nume = this.p1input.value();
        this.p2nume = this.p2input.value();
      }


    });

    this.p1input.position(200, 600);

    this.p2input.position(850, 600);

  }

  showWinnerScreen() { //show winner button and rematch button
    this.buttonRematch.show();
    this.buttonNewGame.show();
  }

  hideWinnerScreen() {
    this.buttonRematch.hide();
    this.buttonNewGame.hide();
    WIN_SCREEN = false;
  }

  display() {
    if (SHOW_OVERLAY) {
      fill(125, 125, 125, 125);
      rect(0, 0, 1200, 900);
      fill('red');
      textSize(32);
      textStyle(BOLD);
      text('Name PlayerX ', 180, 550);
      text('Name PlayerO ', 830, 550);
    }
    if (WIN_SCREEN) {
      fill(125, 125, 125, 125);
      rect(0, 0, 1200, 900);
      textSize(64);
      textStyle(BOLD);
      fill('red');
      text('Winner ' + NUME_CASTIGATOR, 450, 450);
    }

  }



}