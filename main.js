var img;
var t;
var p;
var overlay;
var STARE_GOL = 0,
  STARE_X = 1,
  STARE_O = 2;
var RAND_X = 1;
var RAND_O = 2;
var RAND = RAND_X;

var WIN_SCREEN=false;

var NUME_CASTIGATOR;

var STATE_PENDING = 3;
var STATE_NORMAL = 4;
var GAME_STATE = STATE_NORMAL;
var SHOW_OVERLAY=true;
var patrate = [];
var startX = 300;
var startY = 150;

sageti = [];

var SELECTED_I = -1,
  SELECTED_J = -1;

  /**
   * initializeaza matricea de 5 pe 5 si patratele
   */
function initPatrate() {
  for (let i = 0; i < 5; i++) {
    patrate[i] = [];
    for (let j = 0; j < 5; j++) {
      patrate[i][j] = new Patrat(j * 119 + 1 + startX, i * 119 + 1 + startY);
    }
  }
}

function setup() {
  createCanvas(1200, 900);
  img = loadImage('assets/background4.jpg');
  t = new tablaObiect();
  initPatrate();
  overlay=new Overlay();


  for (let i = 0; i < 4; i++) {
    sageti[i] = new Point(0, 0);
  }
}

function showPatrate() {
  for (let i = 0; i < 5; i++) {
    for (let j = 0; j < 5; j++) {
      patrate[i][j].display();
    }
  }
}

function draw() {
  background(img);
  t.display();
  showPatrate();
  overlay.display();
}
/**
 * returneaza pozitia click-ului translatata in coordonatele matricei de butoane
 */
function getIndex() { 
  for (let i = 0; i < 5; i++) {
    for (let j = 0; j < 5; j++) {
      if (mouseX > patrate[i][j].x && mouseX < patrate[i][j].x + patrate[i][j].width &&
        mouseY > patrate[i][j].y && mouseY < patrate[i][j].y + patrate[i][j].height) {
        return new Point(i, j);
      }
    }
  }
  return new Point(-1, -1);
}

/**
 * Returneaza pe ce sageata a apasat utilizatgorul
 * una din patru sageti posibile:
 * 0-sus
 * 1-dreapta
 * 2-jos
 * 3-stanga
 */
function getSelectedSageata() {

  for (let i = 0; i < 4; i++) {
    if (mouseX > sageti[i].i - 25 && mouseX < sageti[i].i + 25 &&
      mouseY > sageti[i].j - 25 && mouseY < sageti[i].j + 25) {
      return i;
    }
  }
  return -1;
}

/**
 * Returneaza cine a castigat jocul
 */
function getWinner()
{
  for (let i = 0; i < 5; i++) { //verificare castig orizontal
    if (patrate[i][0].stare != STARE_GOL) {
      castig = true;
      for (let j = 0; j < 5; j++) {
        if (patrate[i][j].stare != patrate[i][0].stare) {
          castig = false;
        }
      }
      if (castig) {
        return patrate[i][0].stare;
      }
    }
  }
  for (let i = 0; i < 5; i++) { //verificare castig vertical
    if (patrate[0][i].stare != STARE_GOL) {
      castig = true;
      for (let j = 0; j < 5; j++) {
        if (patrate[j][i].stare != patrate[0][i].stare) {
          castig = false;
        }
      }
      if (castig){
        return patrate[0][i].stare;
      }
    }
  }

  castig = true;
  for (let i = 0; i < 5; i++) { // verificare castig prima diagonala
    if (patrate[i][i].stare != patrate[0][0].stare || patrate[i][i].stare == STARE_GOL) {
      castig = false;
      break;
    }
  }
  if (castig) {
    return patrate[0][0].stare;
  }
  castig = true;
  for (let i = 0; i < 5; i++) { //verificare castig a doua diagonala
    if (patrate[4 - i][i].stare != patrate[0][4].stare || patrate[4 - i][i].stare == STARE_GOL) {
      castig = false;
      break;
    }
  }
  if (castig){
    return patrate[4][0].stare;
  }
  return 'nem';
}

/**
 * Verifica daca a castigat sau nu cineva jocul
 */
function gameOver() {
  castig = false;

  for (let i = 0; i < 5; i++) {
    if (patrate[i][0].stare != STARE_GOL) {
      castig = true;
      for (let j = 0; j < 5; j++) {
        if (patrate[i][j].stare != patrate[i][0].stare) {
          castig = false;
        }
      }
      if (castig) {
        return true;
      }
    }
  }

  for (let i = 0; i < 5; i++) {
    if (patrate[0][i].stare != STARE_GOL) {
      castig = true;
      for (let j = 0; j < 5; j++) {
        if (patrate[j][i].stare != patrate[0][i].stare) {
          castig = false;
        }
      }
      if (castig){
        return true;
      }
    }
  }

  castig = true;
  for (let i = 0; i < 5; i++) {
    if (patrate[i][i].stare != patrate[0][0].stare || patrate[i][i].stare == STARE_GOL) {
      castig = false;
      break;
    }
  }
  if (castig) {
    return true;
  }
  castig = true;
  for (let i = 0; i < 5; i++) {
    if (patrate[4 - i][i].stare != patrate[0][4].stare || patrate[4 - i][i].stare == STARE_GOL) {
      castig = false;
      break;
    }
  }
  if (castig){
    return true;
  }
  return false;
}

/**
 * Verifica daca utilizatorul a dat click pe un patrat care ii apartine
 * sau daca a dat click pe un patrat gol
 * @param {*} _index indexul din matrice pe care a dat click jucatorul
 */
function goodClick(_index) {
  return patrate[_index.i][_index.j].stare == RAND || patrate[_index.i][_index.j].stare == STARE_GOL;
}

/**
 * Functie de reset a tablei de joc
 */
function refreshGame() {
  for (let i = 0; i < 5; i++) {
    for (let j = 0; j < 5; j++) {
      patrate[i][j].stare = STARE_GOL;
    }
  }
  GAME_STATE = STATE_NORMAL;
}

function mousePressed() {
  index = getIndex();

  indexSageti = getSelectedSageata();


  if ((index.i == 0 || index.i == 4 || index.j == 0 || index.j == 4) && goodClick(index)
    && !WIN_SCREEN && !SHOW_OVERLAY) {
    SELECTED_I = index.i;
    SELECTED_J = index.j;
    GAME_STATE = STATE_PENDING;
    for (let i = 0; i < 4; i++) {
      sageti[i].valid = false;
    }
    if (index.i == 0 && index.j == 0) {
      //stanga sus 
      sageti[1] = new Point(119 * 5 + 3 + startX + 25, 10 + startY + 50);
      sageti[2] = new Point(10 + startX + 50, 119 * 5 + 3 + startY + 25);
      sageti[1].valid = true;
      sageti[2].valid = true;
    } else if (index.i == 0 && index.j == 4) {
      //dreapta sus
      sageti[3] = new Point(startX - 25, 10 + startY + 50);
      sageti[2] = new Point(119 * 5 + startX - 119 / 2, 119 * 5 + 3 + startY + 25);
      sageti[3].valid = true;
      sageti[2].valid = true;
    } else if (index.i == 4 && index.j == 0) {
      //stanga jos
      sageti[2] = new Point(119 * 5 + 3 + startX + 25, 119 * 5 + startY - 119 / 2);
      sageti[0] = new Point(startX + 119 / 2, startY - 25);

      sageti[0].valid = true;
      sageti[2].valid = true;
    } else if (index.i == 4 && index.j == 4) {
      //dreapta jos
      sageti[3] = new Point(startX - 25, 119 * 5 + startY - 119 / 2);
      sageti[0] = new Point(startX + 119 * 5 - 119 / 2, startY - 25);

      sageti[3].valid = true;
      sageti[0].valid = true;
    } else {
      if (index.i > 0 && index.i < 4 && index.j == 0) {
        //latura stanga
        //se aprind laturile 0 1 2 adica sus dreapta jos
        sageti[0] = new Point(startX + 119 / 2, startY - 25); //0 -> sus 1->dreapta 2->jos 3->stanga
        sageti[2] = new Point(startX + 119 / 2, startY + 119 * 5 + 25);
        sageti[1] = new Point(startX + 119 * 5 + 25, startY + index.i * 119 + 119 / 2);

        sageti[0].valid = true;
        sageti[2].valid = true;
        sageti[1].valid = true;
      } else if (index.i == 0 && index.j > 0 && index.j < 4) {
        //latura sus
        //se aprind laturile 1 2 3 adica dreapta jos stanga
        sageti[2] = new Point(startX + index.j * 119 + 119 / 2, startY + 119 * 5 + 25);
        sageti[1] = new Point(119 * 5 + 3 + startX + 25, 10 + startY + 50);
        sageti[3] = new Point(startX - 25, startY + 119 / 2);

        sageti[3].valid = true;
        sageti[1].valid = true;
        sageti[2].valid = true;
      } else if (index.i == 4 && index.j > 0 && index.j < 4) {
        //latura jos
        //se aprind laturile 0 1 3 adica sus dreapta stanga
        sageti[0] = new Point(startX + 119 * index.j + 119 / 2, startY - 25);
        sageti[1] = new Point(startX + 119 * 5 + 25, startY + 119 * 5 - 119 / 2);
        sageti[3] = new Point(startX - 25, startY + 119 * 5 - 119 / 2);

        sageti[3].valid = true;
        sageti[1].valid = true;
        sageti[0].valid = true;
      } else if (index.j == 4 && index.i > 0 && index.i < 4) {
        //latura dreapta
        //se aprind laturile 0 2 3 adica sus jos stanga
        sageti[3] = new Point(startX - 25, startY + index.i * 119 + 119 / 2);
        sageti[0] = new Point(startX + 119 * 5 - 119 / 2, startY - 25);
        sageti[2] = new Point(startX + 119 * 5 - 119 / 2, startY + 119 * 5 + 25);

        sageti[3].valid = true;
        sageti[2].valid = true;
        sageti[0].valid = true;
      }
    }
  }

  if (indexSageti != -1 && GAME_STATE == STATE_PENDING) {

    if (indexSageti == 0) {
      for (let i = SELECTED_I; i > 0; i--) {
        patrate[i][SELECTED_J].stare = patrate[i - 1][SELECTED_J].stare;
      }
      patrate[0][SELECTED_J].stare = RAND == RAND_X ? STARE_X : STARE_O;
    } else if (indexSageti == 1) {
      for (let j = SELECTED_J; j < 4; j++) {
        patrate[SELECTED_I][j].stare = patrate[SELECTED_I][j + 1].stare;
      }
      patrate[SELECTED_I][4].stare = RAND == RAND_X ? STARE_X : STARE_O;
    } else if (indexSageti == 2) {
      for (let i = SELECTED_I; i < 4; i++) {
        patrate[i][SELECTED_J].stare = patrate[i + 1][SELECTED_J].stare;
      }
      patrate[4][SELECTED_J].stare = RAND == RAND_X ? STARE_X : STARE_O;
    } else if (indexSageti == 3) {
      for (let j = SELECTED_J; j > 0; j--) {
        patrate[SELECTED_I][j].stare = patrate[SELECTED_I][j - 1].stare;
      }
      patrate[SELECTED_I][0].stare = RAND == RAND_X ? STARE_X : STARE_O;
    }
    GAME_STATE = STATE_NORMAL;
    RAND = RAND == RAND_X ? RAND_O : RAND_X;
  }
  if (gameOver()) {
    print("gata joc");
    winner=getWinner();
    if (winner==STARE_X){
      NUME_CASTIGATOR=overlay.p1input.value();
    }else if (winner==STARE_O){
      NUME_CASTIGATOR=overlay.p2input.value();
    }
    WIN_SCREEN=true;
    overlay.showWinnerScreen();
  }
}

function mouseReleased() {
  //GAME_STATE = STATE_NORMAL;
}