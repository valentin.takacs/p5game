class tablaObiect {

  tablaObiect() {}

  display() {
    fill('#660033');
    circle(600, 450, 850);
    fill('#800040');
    rect(300, 150, 600, 600);
    if (GAME_STATE == STATE_PENDING) {
      push();
      fill('#00ff00');
      strokeWeight(5);
      for (let i=0;i<4;i++){
        if (sageti[i].valid){
          circle(sageti[i].i,sageti[i].j,50);
        }
      }
      
      pop();
    }
  }

}